/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
extern uint8 const CYCODE LCD_Khanas_customFonts[];
int main(void)
{
    uint8 column1 = 4u;
    uint8 row1 = 1u;
    uint8 column2 =15u;
    uint8 row2 = 1u;
    uint8 i = 0u;
    
    LCD_Khanas_Start();
    LCD_Khanas_LoadCustomFonts(LCD_Khanas_customFonts);
    
    

    for(;;)
    {
    LCD_Khanas_ClearDisplay();
    LCD_Khanas_Position(row1, column1);
    LCD_Khanas_PutChar(LCD_Khanas_CUSTOM_0);
 
    LCD_Khanas_Position(row1, column2);
    LCD_Khanas_PutChar(LCD_Khanas_CUSTOM_1);
          
    if( i==0) {
        row1 = (i/8)%2;
        i++;
    } else { 
        if(row1) {
            column1--;
            if(column2==3){
                column2*=4;
            } else {
                column2++;
                i++;
            }
        }
        else {
            column1++;
            if(column2==12) {
                column2/=4;
            } else {
                column2--;
                i++;
            }
        }
        if(i%8==0 ) {
            row1 = (i/8)%2;
            i++;
        }
       
    }
    
    CyDelay(470);
        
    }
}

/* [] END OF FILE */
